set nocompatible

execute pathogen#infect()

if (has("termguicolors"))
	set termguicolors
endif

colorscheme one 

syntax on 

filetype plugin indent on

" NERD Tree 
" Config
let g:NERDTreeGitStatusUseNerdFonts = 1
let g:DevIconsEnableFoldersOpenClose = 1
let g:NERDTreeAutoDeleteBuffer = 1
let g:NERDTreeShowHidden = 1
let g:NERDTreeDirArrows = 1
let g:NERDTreeIgnore = []
let g:NERDTreeStatusline = ''
let g:NERDTreeDirArrowExpandable = ''
let g:NERDTreeDirArrowCollapsible = ''
" Rust-vim
let g:rustfmt_autosave = 1
" Lightline
let g:lightline = {
\   'colorscheme': 'one',
\   'active': {
\     'left': [ 
\       [ 'mode', 'paste' ],
\       [ 'gitbranch', 'readonly', 'filename', 'modified' ] 
\     ]
\   },
\   'component_function': {
\     'gitbranch': 'FugitiveHead'
\   },
\ }

" Keyboard shortcut
" Toggle
nnoremap <silent> <C-t> :NERDTreeToggle<CR>
nnoremap <silent> <C-f> :NERDTreeFind<CR>
" Move between windows
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


set t_Co=256                            " Support 256 colors
set background=dark
set splitbelow                          " Horizontal splits will automatically be below
set splitright                          " Vertical splits will automatically be to the right set tabstop=2                           " Insert 2 spaces for a tab
" set shiftwidth=2                        " Change the number of space characters inserted for indentation
set smarttab                            " Makes tabbing smarter will realize you have 2 vs 4
set expandtab                           " Converts tabs to spaces
set smartindent                         " Makes indenting smart
set autoindent                          " Good auto indent
set clipboard=unnamedplus               " Copy paste between vim and everything else
set number
set noshowmode

" Automaticaly close nvim if NERDTree is only thing left open
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

#!/bin/bash

# Color start/end flags
SCF="\e["
ECF="\e[0m"

# Font weight
# +---------+------+------------+
# | 0       | 1    | 2          |
# +---------+------+------------+
# | regular | bold | underlined |
# +---------+------+------------+
declare -a STYLE
STYLE=("0;" "1;" "4;")

# Foreground & Background
# +-------+-----+-------+--------+------+--------+------+-------+
# | 0     | 1   | 2     | 3      | 4    | 5      | 6    | 7     |
# +-------+-----+-------+--------+------+--------+------+-------+
# | black | red | green | yellow | blue | purple | cyan | white |
# +-------+-----+-------+--------+------+--------+------+-------+
declare -a FG BG
FG=("30m" "31m" "32m" "33m" "34m" "35m" "36m" "37m")
BG=("40;" "41;" "42;" "43;" "44;" "45;" "46;" "47;")

color() {
	#######################################################################
	# DESCRIPTION
	#	Colorize a string
	#
	# USAGE
	#	color ([STYLE]|[FG]){0,2} "my string"
	#
	# STYLE
	#	b	Bold
	#	u	Underlined
	#
	# FG
	#	B	Black
	#	R	Red
	#	G	Green
	#	Y	Yellow
	#	Bl	Blue
	#	P	Purple
	#	C	Cyan
	#
	# N.B.
	#	Default configuration is set to WHITE and REGULAR font
	#######################################################################

	style_idx=0
	fg_idx=8

	case $1 in
	*[b]*) style_idx=1 ;;
	*[u]*) style_idx=2 ;;
	esac

	case $1 in
	*[B]*) fg_idx=0 ;;
	*[R]*) fg_idx=1 ;;
	*[G]*) fg_idx=2 ;;
	*[Y]*) fg_idx=3 ;;
	*[Bl]*) fg_idx=4 ;;
	*[P]*) fg_idx=5 ;;
	*[C]*) fg_idx=6 ;;
	esac

	echo -ne "$SCF${STYLE[$style_idx]}${FG[$fg_idx]}$2$ECF"
}

notify() {
	echo -e $(color P "$*")
}

notify_done() {
	echo -e $(color G 'Done!')
	echo -e $(color bB '---')
	ring_bell
}

ring_bell() {
	echo -ne "\a"
}

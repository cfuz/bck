#!/bin/bash

source $HOME/.config/sh/colors.sh

# Creating `~/.venv` if directory does not exist.
if [[ ! -d ~/.venv ]]; then
	mkdir ~/.venv
fi

pyenv() {
	#######################################################################
	# DESCRIPTION
	#	Activate a python environment (through `venv` manager) given a
	# 	name and asks the user if it should create it, if this name is
	# 	not listed in the existing **Pyenvs**.
	#	Also, if a pyenv is active, the user can stop it with the
	#	`kill` argument.
	#
	# USAGE
	#	pyenv { kill | [del] pyenv-name }
	#
	# PARAMS
	#	kill		Kill the current pyenv if one is active
	#	(OPT) del	Delete the specified **Pyenv**
	#	pyenv-name	Name of the (new) pyenv to load
	#
	# N.B.
	#	The `pyenv-name` will be called from `~/.venv/`. This folder
	#	will be created if it does not already exists.
	#	A tab completion is avalaible to help the user to select an
	#	existing **Pyenv**.
	#######################################################################

	# Checks user input validity
	if (($# > 2)); then
		err_msg=$(echo $(color Y 'Expected '))
		err_msg+=$(echo $(color bG '2'))
		err_msg+=$(echo $(color Y ' argument maximum, gave '))
		err_msg+=$(echo $(color bR "$#"))
		echo >&2 -e $err_msg$(color Y '..')
		return 1
	else
		if (($# == 2)); then
			if [[ "$1" == "kill" ]]; then
				echo >&2 -e $(color Y 'Illegal argument combination provided..')
				return 1
			elif [[ "$1" == "del" ]]; then
				if [[ "$2" == "kill" ]]; then
					echo >&2 -e $(color Y 'Illegal argument combination provided..')
					return 1
				elif [[ ! -d ~/.venv/$2 ]]; then
					echo >&2 -e $(color Y "This pyenv doesn't exist..")
					return 1
				fi
			else
				echo >&2 -e $(color Y 'Illegal argument combination provided..')
				return 1
			fi
		elif (($# == 1)); then
			if [[ "$1" == "del" ]]; then
				echo >&2 -e $(color Y 'Expected a pyenv to delete..')
				return 1
			elif [[ "$1" == "kill" && -z $VIRTUAL_ENV ]]; then
				echo >&2 -e $(color Y "No pyenv set..")
				return 1
			fi
		else
			echo >&2 -e $(color Y 'Not enough argument..')
			return 1
		fi
	fi

	case "$1" in
	del)
		rm -rf ~/.venv/$2
		echo -ne $(color G 'Pyenv ')
		echo -ne $(color bG "$1")
		echo -e $(color G ' deleted!')
		;;
	kill)
		deactivate
		echo -e $(color Y 'Pyenv stopped!')
		;;
	*)
		if [[ -n $VIRTUAL_ENV && $1 = $(echo $VIRTUAL_ENV | rev | cut -f1 -d'/' | rev) ]]; then
			echo -e $(color C "Pyenv already set!")
		else
			# Pyenv existence check
			activate=true
			if [[ ! -d ~/.venv/$1 ]]; then
				echo -ne $(color Y "Pyenv ")
				echo -ne $(color bY "$1")
				echo -e $(color Y " does not exist!")
				while true; do
					echo -en "Create pyenv? [$(color b 'Y')/n] "
					read usr_in
					case $usr_in in
					[Yy]*)
						python3 -m venv ~/.venv/$1
						echo -e $(color G "Pyenv created successfully!")
						break
						;;
					[Nn]*)
						echo -e "Aborted process.."
						activate=false
						break
						;;
					*)
						echo -e "Please answer $(color bB 'yes') or $(color bB 'no').."
						;;
					esac
				done
			fi

			# Pyenv activation management
			if $activate; then
				source ~/.venv/$1/bin/activate
				echo -e $(color G 'Pyenv fired!')
			fi
		fi
		;;
	esac

	return 0
}

_pyenv_completion() {
	#######################################################################
	# DESCRIPTION
	#	Lists the available **Pyenv** options for completion purposes.
	#
	# N.B.
	#	This function is private.
	#	Since the pyenv function only accepts one argument, we prevent
	#	the completion process from computing when one argument is
	#	given.
	#######################################################################

	# Checks the number of args entered and cut the completion process if
	# more than one is given
	if (($COMP_CWORD > 2)); then
		return
	fi

	# Current input	to lead the completion `COMP_CWORD`
	local cur=${COMP_WORDS[COMP_CWORD]}
	# Gets all dirnames from `~/.venv`
	local opt="$(ls -dX ~/.venv/* | rev | cut -f1 -d'/' | rev)"

	if (($COMP_CWORD == 2)); then
		if [[ "${COMP_WORDS[1]}" == "del" ]]; then
			COMPREPLY=($(compgen -W "$opt" -- "$cur"))
		else
			return
		fi
	else
		opt+=" del"
		# Checks if a Pyenv is currently running to add a kill option.
		opt+=" $([[ -n $VIRTUAL_ENV ]] && echo 'kill')"
		# Retrieves the list of options given the current value of the
		# first argument.
		COMPREPLY=($(compgen -W "$opt" -- "$cur"))
	fi
}

# For dynamic completion behavior
complete -F _pyenv_completion pyenv

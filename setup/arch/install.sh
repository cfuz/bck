#!/bin/bash

# Path to script's directory
BCKDIR=$(dirname $0)
CURPATH=$PWD
SWFILE=$BCKDIR/base-deps.txt
NVHW=$(lspci | egrep -i 'geforce [g|r]tx [[:digit:]]*' | cut -d'[' -f2 | cut -d']' -f1)

declare -a SW

# Base home directory architecture
mkdir -p $HOME/web $HOME/dev $HOME/dl $HOME/img $HOME/vid $HOME/doc
mkdir -p $HOME/.config $HOME/.venv $HOME/.node $HOME/.local/share/fonts

# Wallpapers and profile pictures
cp -r $BCKDIR/../../img/* $HOME/img

# Fonts (comment if only in TTY mode)
cp $BCKDIR/../../fonts/* $HOME/.local/share/fonts

# Shell basic aliases and constants
mkdir -p $HOME/.config/sh
cp $BCKDIR/../../etc/sh/* $HOME/.config/sh
echo '. $HOME/.config/sh/consts' >> $HOME/.bashrc
echo '. $HOME/.config/sh/aliases' >> $HOME/.bashrc
echo '. $HOME/.config/sh/colors.sh' >> $HOME/.bashrc
echo '. $HOME/.config/sh/venv.sh' >> $HOME/.bashrc
echo '[[ $TERM == linux  ]] && setvtrgb $HOME/.config/tty/colormap.txt' >> $HOME/.bashrc
source $HOME/.bashrc

# System upgrade
pacman -Syy && pacman -Syu

# Basic software installation
while read soft; do SW+=($soft); done < $SWFILE
[[ -n $NVHW ]] && SW+=("nvidia" "nvidia-utils")
sudo pacman -S "${SW[@]}"

# Neovim
mkdir -p $HOME/.config/nvim/autoload $HOME/.config/nvim/bundle
touch $HOME/.config/nvim/init.vim
cp $BCKDIR/../../etc/nvim/* $HOME/.config/nvim
curl -LSso $HOME/.config/nvim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
cd $HOME/.config/nvim/bundle/
git clone https://github.com/scrooloose/nerdtree
git clone https://github.com/itchyny/lightline.vim
git clone https://github.com/tpope/vim-fugitive
git clone https://github.com/rust-lang/rust.vim
git clone https://github.com/Xuyuanp/nerdtree-git-plugin.git
git clone https://github.com/ryanoasis/vim-devicons.git
git clone https://github.com/rakr/vim-one
cd $CURPATH

# Rust
mkdir -p $HOME/.config/rust
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
rustup update
rustc --version
rustup component add rustfmt rust-src
cp $BCKDIR/../../etc/rust/* $HOME/.config/rust

# Node.js
tar -xvf $BCKDIR/../../node-v16.13.0-linux-x64.tar.xz --directory $HOME/.node
mv $HOME/.node/node-v16.13.0-linux-x64/*  $HOME/.node/
rm -rf $HOME/.node/node-v16.13.0-linux-x64


# Ranger
git clone https://github.com/hut/ranger.git
cd ranger
sudo make install

# Git
git config --global core.editor "nvim"
git config --global user.name "cfuz"
git config --global user.email "cfuz@pm.me"


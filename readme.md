# Backup

To install the basic softwares and dependencies on a new linux device, simply head to the `setup/[deb|arch]` and fire the `install.sh` script.

# Some useful links to check on a new setup

## Terminal color scheme

On a desktop environment, you can customize the terminal emulator thanks to the [Gogh](https://mayccoll.github.io/Gogh/) colorschemes bank.
Simply spot the appropriate schemes you want to install on your device and input the following command:

```sh
bash -c "$(wget -qO- https://git.io/vQgMr)"
```

## Vim customization

### NeoVim vs. Vim

[Neovim Vs Vim: The differences Compared (Quick)](https://hostingdonuts.com/neovim-vs-vim/)

### Setup

#### Tree

```
.config
└── [n]vim
    ├── autoload
    ├── bundle
    └── [init.vim]
```

The `bundle/` directory should contain all the extensions.
Here are the basic extra-features I use:

- lightline
- vim-devicons
- vim-fugitive
- nerdtree
- nerdtree-git-plugin
- vim-devicons
- vim-one

#### Pathogen

Then install Pathogen using the tutorial from the [GitHub page](https://github.com/tpope/vim-pathogen) of this vim plugin manager

### Configuration

See the `init.vim` file provided in this repository as a basic vim configuration.

### Onivim

[Onivim](https://www.onivim.io/) is a fast and lightweight vim-based code editor.

## Server configuration

### Set the color map for all virtual terminal sesions (**TTY**)

```
setvtrgb FILE
```

Where `FILE` gives the following set of lines:

```
color0_r,color1_r,...,color15_r
color0_g,color1_g,...,color15_g
color0_b,color1_b,...,color15_b
```

## Note

Here, `colorN_{r,g,b}` is the red/green/blue portion of the respective color in decimal notation in the `0..255` range.
To pre-seed a file in the correct format, you can use:

```
cat /sys/module/vt/parameters/default_{red,grn,blu}
```

The meanings of the color values are defined as follows:

```
+--------+--------+---------+
| Normal | Bright | Color   |
+--------+--------+---------+
|      0 |      8 | Black   |
|      1 |      9 | Red     |
|      2 |     10 | Green   |
|      3 |     11 | Yellow  |
|      4 |     12 | Blue    |
|      5 |     13 | Magenta |
|      6 |     14 | Cyan    |
|      7 |     15 | White   |
+---------------------------+
```

For instance the `etc/tty/colormap.txt` file defines the RGB color sequence as defined in the [terminal.sexy](https://terminal.sexy/) default color map.
To set it up just input the following command line:

```sh
setvtrgb etc/tty/colormap.txt
```

Restart your device and voilà!

## Custom font

On a desktop environment you can provide custom fonts in the `~/.local/share/fonts` folder:

```
mkdir -p ~/.local/share/fonts
cp path/to/font.ttf ~/.local/share/fonts
```

To reload the font manager cache:
```
sudo fc-cache -f -v
```